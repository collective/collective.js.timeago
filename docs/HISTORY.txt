Changelog
=========

1.1 (unreleased)
----------------

- Activate timeago on '.timeago' jQuery selector.


1.0 (2012-10-20)
----------------

- Initial release
